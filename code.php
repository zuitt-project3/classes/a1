<?php


class Person {
	public $firstName;
	public $middleName;
	public $lastName;

	public function __construct($firstName, $middleName, $lastName)
	{
		$this->firstName =$firstName;
		$this->middleName = $middleName;
		$this->lastName = $lastName;
	}

	public function getDetails()
	{
		return "Your Full Name is $this->firstName $this->lastName.";
	}
}

class Developer extends Person {
	public function getDetails (){
		return "Your name is $this->firstName $this->middleName $this->lastName and your are a developer.";
	}
}


class Engineer extends Person{
	public function getDetails (){
		return "You are an engineer name $this->firstName $this->middleName $this->lastName.";
	}
}


$person = new Person ("Senku", "", "Ishigami");
$dev = new Developer ("John", "Finch", "Smith");
$engr = new Engineer ("Harold", "Myers", "Reese");